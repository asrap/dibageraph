# Dibageraph

This a test project written with python/djangorest.
Tried to implement a very simple commenting mechanism.

## Getting Started

Clone the project.
Create a python virtualenv.
Install the requirements.
You are ready to go...

### Prerequisites

You need to install Django, Djangorestframework and coverage for testing purposes.


```
pip install -r requirements.txt
```

### *Note
Calling APIs needs an authenticated user.
In order to get a token for an existing user, send username and password to 
'get_token/' in JSON format



## Running the tests

Run this commands:
```
coverage erase
coverage run --branch --parallel-mode manage.py test comments
coverage combine
coverage html
```
A htmlcov directory will be created in project root.
Open the directory and find index.html file.
Open this file in browser to see the testing reports

## Authors

* **Name: Parsa Eini** 
* **Email: parsaeini1998@gmail.com** 
