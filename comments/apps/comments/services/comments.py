from comments.apps.comments.models import Comment
from comments.apps.comments.services.posts import PostService


class CommentService(object):

    @staticmethod
    def create_comment(user, post, text):
        return Comment.objects.create(user=user, post=post, text=text)

    @staticmethod
    def comment_exists(**kwargs):
        return Comment.objects.filter(**kwargs).exists()

    @staticmethod
    def get_comments(post_id):
        post = PostService.get_post_by_id(post_id)
        return post.comments.all()
