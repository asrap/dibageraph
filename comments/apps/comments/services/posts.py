from comments.apps.comments.models import Post


class PostService(object):

    @staticmethod
    def get_post(*args, **kwargs):
        try:
            return Post.objects.get(*args, **kwargs)
        except Post.DoesNotExist:
            return None

    @staticmethod
    def get_post_by_id(id_):
        return PostService.get_post(id=id_)

    @staticmethod
    def create_post(user, text):
        return Post.objects.create(user=user, text=text)


