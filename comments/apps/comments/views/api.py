from django.utils.functional import cached_property
from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet

from comments.apps.comments.serializers import CommentSerializer
from comments.apps.comments.services.comments import CommentService
from comments.apps.comments.services.posts import PostService


class CommentAPIView(ModelViewSet):
    permission_classes = [permissions.IsAuthenticated, ]
    lookup_field = "id"
    serializer_class = CommentSerializer

    @cached_property
    def post_object(self):
        return PostService.get_post_by_id(self.kwargs.get("post_id"))

    def get_queryset(self):
        return CommentService.get_comments(self.kwargs.get('post_id'))

    def perform_create(self, serializer):
        serializer.save(
            user=self.request.user,
            post=self.post_object
        )

    def perform_update(self, serializer):
        serializer.save(
            user=self.request.user,
            post=self.post_object
        )
