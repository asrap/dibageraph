from django.urls import path

from ..views.api import CommentAPIView

app_name = 'comments'

urlpatterns = [
    path('<int:post_id>/comments/', CommentAPIView.as_view({'post': 'create', 'get': 'list'}), name='comments'),
    path('<int:post_id>/comments/<int:id>/', CommentAPIView.as_view(
        {'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='comment'
         ),
]
