from django.contrib.auth import get_user_model
from django.forms import model_to_dict
from django.test import TestCase

from comments.apps.comments.services.comments import CommentService
from comments.apps.comments.services.posts import PostService

UserModel = get_user_model()


class ServiceTestBase(TestCase):
    def setUp(self):
        self.user = UserModel.objects.create(username="test_username")
        self.post = PostService.create_post(user=self.user, text="test_post_text")


class CommentServiceTestCase(ServiceTestBase):

    def test_create_comment(self):
        kwargs = {"user": self.user, "post": self.post, "text": "test_comment_text"}
        comment = CommentService.create_comment(**kwargs)

        for (key, value) in kwargs.items():
            self.assertEqual(getattr(comment, key), value)

    def test_comment_exists_true(self):
        kwargs = {"user": self.user, "post": self.post, "text": "test_comment_text"}
        CommentService.create_comment(**kwargs)
        self.assertTrue(CommentService.comment_exists(**kwargs))

    def test_comment_exists_false(self):
        kwargs = {"text": "test_does_not_exist"}
        self.assertFalse(CommentService.comment_exists(**kwargs))

    def test_get_post_all_comments(self):
        CommentService.create_comment(user=self.user, post=self.post, text="test_text1")
        CommentService.create_comment(user=self.user, post=self.post, text="test_text2")

        comments = CommentService.get_comments(self.post.id)

        self.assertEqual(len(comments), 2)
        for comment in comments:
            self.assertEqual(comment.post.id, self.post.id)


class PostServiceTestCase(ServiceTestBase):

    def test_create(self):
        kwargs = {"user": self.user, "text": "test_text"}
        post = PostService.create_post(**kwargs)

        for (key, value) in kwargs.items():
            self.assertEqual(getattr(post, key), value)

    def test_get_post_by_id_exists(self):
        post = PostService.get_post_by_id(self.post.id)
        self.assertEqual(model_to_dict(post), model_to_dict(self.post))

    def test_get_post_by_id_none(self):
        self.assertIsNone(PostService.get_post_by_id(1000))
