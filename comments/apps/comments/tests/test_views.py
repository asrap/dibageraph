import json

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from django.utils.encoding import force_text
from rest_framework import status
from rest_framework.test import APIClient

from comments.apps.comments.services.comments import CommentService
from comments.apps.comments.services.posts import PostService

User_Model = get_user_model()


class CommentAPIViewTestCase(TestCase):
    def setUp(self):
        self.user = User_Model.objects.create(username="test_username", password="test_pass")
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)
        self.client.login(username="test_username", password="test_pass")

        self.post = PostService.create_post(user=self.user, text="post_text")

    def test_post(self):
        expected_data = {
            "id": 1,
            "post": {
                "id": self.post.id,
                "text": self.post.text
            },
            "text": "test_comment_text"
        }

        url = reverse("comment:comments", kwargs={"post_id": self.post.id})
        data = {
            "text": "test_comment_text"
        }
        response = self.client.post(url, data=data)

        response_data = json.loads(force_text(response.content))

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertDictEqual(
            response_data, expected_data
        )

    def test_post_invalid_data(self):
        url = reverse("comment:comments", kwargs={"post_id": self.post.id})
        response = self.client.post(url, data={})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_put(self):
        expected_data = {
            "id": 1,
            "post": {
                "id": self.post.id,
                "text": self.post.text
            },
            "text": "edited_comment_text"
        }

        comment = CommentService.create_comment(user=self.user, post=self.post, text="comment_text")
        url = reverse("comment:comment", kwargs={"post_id": self.post.id, "id": comment.id})
        data = {
            "text": "edited_comment_text"
        }
        response = self.client.put(url, data=data)

        response_data = json.loads(force_text(response.content))

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertDictEqual(
            response_data, expected_data
        )

        comment.refresh_from_db()
        self.assertEqual(comment.text, expected_data["text"])

    def test_put_invalid_data(self):
        comment = CommentService.create_comment(user=self.user, post=self.post, text="comment_text")
        url = reverse("comment:comment", kwargs={"post_id": self.post.id, "id": comment.id})
        response = self.client.put(url, data={})

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete(self):
        comment = CommentService.create_comment(user=self.user, post=self.post, text="comment_text")
        url = reverse("comment:comment", kwargs={"post_id": self.post.id, "id": comment.id})
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(CommentService.comment_exists(user=self.user, post=self.post, text="comment_text"))

    def test_list(self):
        comment_1 = CommentService.create_comment(user=self.user, post=self.post, text="comment_text1")
        comment_2 = CommentService.create_comment(user=self.user, post=self.post, text="comment_text2")
        url = reverse("comment:comments", kwargs={"post_id": self.post.id})
        response = self.client.get(url)
        response_data = json.loads(force_text(response.content))
        expected_data = {
            'count': 2,
            'next': None,
            'previous': None,
            'results': [
                {
                    "id": 1,
                    "post": {
                        "id": self.post.id,
                        "text": self.post.text
                    },
                    "text": comment_1.text
                },
                {
                    "id": 2,
                    "post": {
                        "id": self.post.id,
                        "text": self.post.text
                    },
                    "text": comment_2.text
                }
            ]
        }

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response_data, expected_data)
