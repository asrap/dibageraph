from django.test import TestCase
from django.contrib.auth import get_user_model

from comments.apps.comments.services.comments import CommentService
from comments.apps.comments.services.posts import PostService

User_Model = get_user_model()


class ModelTestBase(TestCase):
    def setUp(self):
        self.user = User_Model.objects.create(
            username="test_username",
        )
        self.post = PostService.create_post(user=self.user, text="test_post_text")


class CommentTestCase(ModelTestBase):

    def test_str(self):
        comment_text = "test_comment_text"
        comment = CommentService.create_comment(
            user=self.user, post=self.post, text=comment_text
        )
        self.assertEqual(str(comment), comment_text)


class PostTestCase(ModelTestBase):
    def test_str(self):
        text = "test_text"
        post = PostService.create_post(user=self.user, text=text)
        self.assertEqual(str(post), text)
