from django.contrib import admin

from comments.apps.comments.models import Comment, Post

admin.site.register(Comment)
admin.site.register(Post)
