from django.contrib import admin
from django.urls import path, include
from rest_framework.authtoken import views

urlpatterns = [
    path('posts/', include('comments.apps.comments.urls.api', 'comment')),
    path('get_token/', views.obtain_auth_token),
    path('admin/', admin.site.urls),
]
